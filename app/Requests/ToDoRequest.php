<?php

namespace App\Requests;

use Illuminate\Foundation\HTTP\FormRequest;

class ToDoRequest extends FormRequest
{
    public function rules(){
        return [
            'title' => 'required|string|max:255',
            'description' => 'required|string|max:255',
        ]
    }
}